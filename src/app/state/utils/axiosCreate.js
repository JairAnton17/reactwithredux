import { axios } from "axios";
import { URL } from './endpoint.js';

export default axios.create({
    baseURL: URL
})