import _ from 'lodash';
import { axiosCreate } from './axiosCreate';
import constantsEndpoint from '../utils/endpoint';


const createRequest = ({ url, data = {} }) => {
    const request = data;
    console.log(url)
    let responseAmplify = {
        statusCode: 500,
        dataJson: null,
    }

    return new Promise(async (resolve, reject) => {
        try
        {
            const response = await axiosCreate.post(url,{ request });
            console.log("response")
            console.log(response)
            resolve(response)
        }
        catch(error)
        {
            console.log("error")
            console.log(error)
        }
    })
}

export const createAPI = urls =>
  Object.keys(urls).reduce(
    (acc, key) => ({ ...acc, [key]: createRequest(urls[key]) }),
    {}
  );

export default createRequest