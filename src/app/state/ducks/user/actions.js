import { constants } from './types.js';

export const login = () => ({
    type: constants.LOGIN
});

export const authenticatedLogin = () => ({
    type: constants.DATA_AUTHENTICATED
});

export const setAuthenticatedLogin = (dataAuth) => ({
    type: constants.SET_DATA_AUTHENTICATED,
    dataAuth
});