import { constants } from "./types.js";
import { createReducer } from '../../utils';

const initialState = {
    isAuthenticated: false
}

const user = createReducer(initialState)({
    
    [constants.DATA_AUTHENTICATED]: (state, action) => {
        const { account } = action.dataAuth;

        const auth = { account }
        return {
            ...state,
            auth
        }
    },

});

export default user;