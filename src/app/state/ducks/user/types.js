import keyMirror from 'keymirror';

export const constants = keyMirror({
    LOGIN: null,

    DATA_AUTHENTICATED: null,
    SET_DATA_AUTHENTICATED: null,
});